﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Tasks
{
    public partial class MainForm : Form
    {
        private TasksDataSetTableAdapters.T_ASSIGNMENTTableAdapter assignmentAdapter;
        private TasksDataSetTableAdapters.V_PERFORMER_TASKSTableAdapter ptAdapter;
        private TasksDataSetTableAdapters.GET_PERFORMER_ID_WITH_MIN_TASKSTableAdapter getMinIDProc;

        public MainForm ()
        {
            InitializeComponent();

            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            string appPath = Application.StartupPath;
            string dbPath = appPath + "\\database\\TASKS.FDB";
            FbConnectionStringBuilder cs = new FbConnectionStringBuilder();
            cs.DataSource = "127.0.0.1";
            cs.Database = dbPath;
            cs.UserID = "SYSDBA";
            cs.Password = "masterkey";
            cs.Charset = "NONE";
            cs.Pooling = false;

            bool onClosing = false;

            FbConnection connection = new FbConnection(cs.ToString());
            try
            {
                connection.Open();
                connection.Close();

                connectionLabel.Text = "Статус: " + cs.DataSource + "; Пользователь: " + cs.UserID;
                this.connectionLabel.Image = global::Tasks.Properties.Resources.ok;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка соединения с базой данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connectionLabel.Text = "Статус: соединение не установлено";
                this.connectionLabel.Image = global::Tasks.Properties.Resources.error;

                onClosing = true;
            }

            if (onClosing)
            {
                Close();
            }
            else
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
                connectionStringsSection.ConnectionStrings["Tasks.Properties.Settings.TasksConnectionString"].ConnectionString =
    cs.ToString();
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");
            }

            assignmentAdapter = new TasksDataSetTableAdapters.T_ASSIGNMENTTableAdapter();
            assignmentAdapter.ClearBeforeFill = true;

            ptAdapter = new TasksDataSetTableAdapters.V_PERFORMER_TASKSTableAdapter();
            ptAdapter.ClearBeforeFill = true;

            getMinIDProc = new TasksDataSetTableAdapters.GET_PERFORMER_ID_WITH_MIN_TASKSTableAdapter();
        }

        public DataTable GetTable (FbCommand cmd)
        {
            System.Data.ConnectionState original = cmd.Connection.State;
            if (cmd.Connection.State == ConnectionState.Closed)
            {
                cmd.Connection.Open();
            }

            DataTable dt = new DataTable();
            FbDataReader dr;

            try
            {
                dr = cmd.ExecuteReader();
                dt.Load(dr);
                dr.Close();
                dr.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте корректность введенных данных и фильтров!" + Environment.NewLine + ex.Message,
                    "Ошибка выполнения запроса", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (cmd.Connection.State == ConnectionState.Open)
            {
                cmd.Connection.Close();
            }

            return dt;
        }

        private void btnAddTask_Click (object sender, EventArgs e)
        {
            AddTaskForm addTaskForm = new AddTaskForm();
            if (addTaskForm.ShowDialog() == DialogResult.OK)
            {
                assignTask(addTaskForm.TaskID, addTaskForm.Time, addTaskForm.Category);
            }
        }

        private void assignTask (int taskID, decimal time, int type)
        {
            try
            {
                TasksDataSet.GET_PERFORMER_ID_WITH_MIN_TASKSDataTable table = this.getMinIDProc.GetData(type);
                TasksDataSet.GET_PERFORMER_ID_WITH_MIN_TASKSRow row = table.Rows[0] as TasksDataSet.GET_PERFORMER_ID_WITH_MIN_TASKSRow;
                int performerID = (int)(long)row.PERFORMER_ID;

                assignmentAdapter.Insert(taskID, performerID);

                this.v_ASSIGNMENTTableAdapter.Fill(this.tasksDataSet.V_ASSIGNMENT);
                this.ptAdapter.Fill(this.tasksDataSet.V_PERFORMER_TASKS);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
            updateChart();
        }

        private void MainForm_Load (object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'tasksDataSet.T_POSITION' table. You can move, or remove it, as needed.
                this.t_POSITIONTableAdapter.Fill(this.tasksDataSet.T_POSITION);
                // TODO: This line of code loads data into the 'tasksDataSet.V_ASSIGNMENT' table. You can move, or remove it, as needed.
                this.v_ASSIGNMENTTableAdapter.Fill(this.tasksDataSet.V_ASSIGNMENT);
                // TODO: This line of code loads data into the 'tasksDataSet.T_PERFORMER' table. You can move, or remove it, as needed.
                this.t_PERFORMERTableAdapter.Fill(this.tasksDataSet.T_PERFORMER);

                this.assignmentAdapter.Fill(this.tasksDataSet.T_ASSIGNMENT);

                this.ptAdapter.Fill(this.tasksDataSet.V_PERFORMER_TASKS);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void updateChart ()
        {
            decimal[] yValues;
            string[] xValues;
            chtTiming.Series[0].Points.Clear();
            chtTiming.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            chtTiming.Series[0].IsValueShownAsLabel = true;
            chtTiming.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            try
            {
                TasksDataSet.V_PERFORMER_TASKSDataTable table = ptAdapter.GetData();

                yValues = new decimal[table.Rows.Count];
                xValues = new string[table.Rows.Count];
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    TasksDataSet.V_PERFORMER_TASKSRow row = table.Rows[i] as TasksDataSet.V_PERFORMER_TASKSRow;
                    yValues[i] = row.PERFORMER_TASK_TIME;
                    xValues[i] = row.PERFORMER_NAME;
                }
                chtTiming.Series[0].Points.DataBindXY(xValues, yValues);
                panel2.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void exitToolStripMenuItem_Click (object sender, EventArgs e)
        {
            this.Close();
        }

        private void typesToolStripMenuItem_Click (object sender, EventArgs e)
        {
            TypesForm types = new TypesForm();
            if (types.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void t_PERFORMERBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.t_PERFORMERBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.tasksDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void bindingNavigatorAddNewItem_Click (object sender, EventArgs e)
        {
            AddPerformerForm addPerformerForm = new AddPerformerForm();
            if (addPerformerForm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.v_ASSIGNMENTTableAdapter.Fill(this.tasksDataSet.V_ASSIGNMENT);
                    this.t_PERFORMERTableAdapter.Fill(this.tasksDataSet.T_PERFORMER);
                }
                catch (Exception ex)
                {
                    MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                        "Ошибка", ex.Message);
                    message.ShowDialog();
                }
            }
        }

        private void positionsToolStripMenuItem_Click (object sender, EventArgs e)
        {
            PositionForm positionForm = new PositionForm();
            positionForm.ShowDialog();

            try
            {
                this.v_ASSIGNMENTTableAdapter.Fill(this.tasksDataSet.V_ASSIGNMENT);
                this.t_PERFORMERTableAdapter.Fill(this.tasksDataSet.T_PERFORMER);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void btnUpdateChart_Click (object sender, EventArgs e)
        {
            updateChart();
        }

        private void toolStripButtonUpdate_Click (object sender, EventArgs e)
        {
            try
            {
                this.v_ASSIGNMENTTableAdapter.Fill(this.tasksDataSet.V_ASSIGNMENT);
                this.t_PERFORMERTableAdapter.Fill(this.tasksDataSet.T_PERFORMER);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }
    }
}

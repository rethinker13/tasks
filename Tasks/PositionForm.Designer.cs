﻿namespace Tasks
{
    partial class PositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PositionForm));
            this.tasksDataSet = new Tasks.TasksDataSet();
            this.t_POSITIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_POSITIONTableAdapter = new Tasks.TasksDataSetTableAdapters.T_POSITIONTableAdapter();
            this.tableAdapterManager = new Tasks.TasksDataSetTableAdapters.TableAdapterManager();
            this.t_POSITIONBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_POSITIONBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPositionTypes = new System.Windows.Forms.ToolStripButton();
            this.t_POSITIONDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tTYPEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_TYPETableAdapter = new Tasks.TasksDataSetTableAdapters.T_TYPETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONBindingNavigator)).BeginInit();
            this.t_POSITIONBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTYPEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tasksDataSet
            // 
            this.tasksDataSet.DataSetName = "TasksDataSet";
            this.tasksDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_POSITIONBindingSource
            // 
            this.t_POSITIONBindingSource.DataMember = "T_POSITION";
            this.t_POSITIONBindingSource.DataSource = this.tasksDataSet;
            // 
            // t_POSITIONTableAdapter
            // 
            this.t_POSITIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.T_ASSIGNMENTTableAdapter = null;
            this.tableAdapterManager.T_PERFORMERTableAdapter = null;
            this.tableAdapterManager.T_POSITION_TYPETableAdapter = null;
            this.tableAdapterManager.T_POSITIONTableAdapter = this.t_POSITIONTableAdapter;
            this.tableAdapterManager.T_TASKTableAdapter = null;
            this.tableAdapterManager.T_TYPETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Tasks.TasksDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_POSITIONBindingNavigator
            // 
            this.t_POSITIONBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_POSITIONBindingNavigator.BindingSource = this.t_POSITIONBindingSource;
            this.t_POSITIONBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_POSITIONBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_POSITIONBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_POSITIONBindingNavigatorSaveItem,
            this.toolStripSeparator1,
            this.tsbPositionTypes});
            this.t_POSITIONBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_POSITIONBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_POSITIONBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_POSITIONBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_POSITIONBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_POSITIONBindingNavigator.Name = "t_POSITIONBindingNavigator";
            this.t_POSITIONBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_POSITIONBindingNavigator.Size = new System.Drawing.Size(743, 25);
            this.t_POSITIONBindingNavigator.TabIndex = 0;
            this.t_POSITIONBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_POSITIONBindingNavigatorSaveItem
            // 
            this.t_POSITIONBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_POSITIONBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_POSITIONBindingNavigatorSaveItem.Image")));
            this.t_POSITIONBindingNavigatorSaveItem.Name = "t_POSITIONBindingNavigatorSaveItem";
            this.t_POSITIONBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_POSITIONBindingNavigatorSaveItem.Text = "Save Data";
            this.t_POSITIONBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_POSITIONBindingNavigatorSaveItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbPositionTypes
            // 
            this.tsbPositionTypes.Image = global::Tasks.Properties.Resources.if_sign_add_299068;
            this.tsbPositionTypes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPositionTypes.Name = "tsbPositionTypes";
            this.tsbPositionTypes.Size = new System.Drawing.Size(92, 22);
            this.tsbPositionTypes.Text = "Типы работ";
            this.tsbPositionTypes.Click += new System.EventHandler(this.tsbPositionTypes_Click);
            // 
            // t_POSITIONDataGridView
            // 
            this.t_POSITIONDataGridView.AutoGenerateColumns = false;
            this.t_POSITIONDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_POSITIONDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.t_POSITIONDataGridView.DataSource = this.t_POSITIONBindingSource;
            this.t_POSITIONDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_POSITIONDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_POSITIONDataGridView.Name = "t_POSITIONDataGridView";
            this.t_POSITIONDataGridView.Size = new System.Drawing.Size(743, 419);
            this.t_POSITIONDataGridView.TabIndex = 1;
            this.t_POSITIONDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.t_POSITIONDataGridView_CellEndEdit);
            this.t_POSITIONDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.t_POSITIONDataGridView_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "POSITION_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "POSITION_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "POSITION_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Должность";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // tTYPEBindingSource
            // 
            this.tTYPEBindingSource.DataMember = "T_TYPE";
            this.tTYPEBindingSource.DataSource = this.tasksDataSet;
            // 
            // t_TYPETableAdapter
            // 
            this.t_TYPETableAdapter.ClearBeforeFill = true;
            // 
            // PositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 444);
            this.Controls.Add(this.t_POSITIONDataGridView);
            this.Controls.Add(this.t_POSITIONBindingNavigator);
            this.Name = "PositionForm";
            this.Text = "Должности";
            this.Load += new System.EventHandler(this.PositionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONBindingNavigator)).EndInit();
            this.t_POSITIONBindingNavigator.ResumeLayout(false);
            this.t_POSITIONBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_POSITIONDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTYPEBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TasksDataSet tasksDataSet;
        private System.Windows.Forms.BindingSource t_POSITIONBindingSource;
        private TasksDataSetTableAdapters.T_POSITIONTableAdapter t_POSITIONTableAdapter;
        private TasksDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_POSITIONBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_POSITIONBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView t_POSITIONDataGridView;
        private System.Windows.Forms.BindingSource tTYPEBindingSource;
        private TasksDataSetTableAdapters.T_TYPETableAdapter t_TYPETableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ToolStripButton tsbPositionTypes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}
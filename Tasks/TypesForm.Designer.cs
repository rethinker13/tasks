﻿namespace Tasks
{
    partial class TypesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TypesForm));
            this.tasksDataSet = new Tasks.TasksDataSet();
            this.t_TYPEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_TYPETableAdapter = new Tasks.TasksDataSetTableAdapters.T_TYPETableAdapter();
            this.tableAdapterManager = new Tasks.TasksDataSetTableAdapters.TableAdapterManager();
            this.t_TYPEBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_TYPEBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.t_TYPEDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEBindingNavigator)).BeginInit();
            this.t_TYPEBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tasksDataSet
            // 
            this.tasksDataSet.DataSetName = "TasksDataSet";
            this.tasksDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_TYPEBindingSource
            // 
            this.t_TYPEBindingSource.DataMember = "T_TYPE";
            this.t_TYPEBindingSource.DataSource = this.tasksDataSet;
            // 
            // t_TYPETableAdapter
            // 
            this.t_TYPETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.T_ASSIGNMENTTableAdapter = null;
            this.tableAdapterManager.T_PERFORMERTableAdapter = null;
            this.tableAdapterManager.T_POSITION_TYPETableAdapter = null;
            this.tableAdapterManager.T_POSITIONTableAdapter = null;
            this.tableAdapterManager.T_TASKTableAdapter = null;
            this.tableAdapterManager.T_TYPETableAdapter = this.t_TYPETableAdapter;
            this.tableAdapterManager.UpdateOrder = Tasks.TasksDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_TYPEBindingNavigator
            // 
            this.t_TYPEBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_TYPEBindingNavigator.BindingSource = this.t_TYPEBindingSource;
            this.t_TYPEBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_TYPEBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_TYPEBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_TYPEBindingNavigatorSaveItem});
            this.t_TYPEBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_TYPEBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_TYPEBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_TYPEBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_TYPEBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_TYPEBindingNavigator.Name = "t_TYPEBindingNavigator";
            this.t_TYPEBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_TYPEBindingNavigator.Size = new System.Drawing.Size(641, 25);
            this.t_TYPEBindingNavigator.TabIndex = 0;
            this.t_TYPEBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_TYPEBindingNavigatorSaveItem
            // 
            this.t_TYPEBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_TYPEBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_TYPEBindingNavigatorSaveItem.Image")));
            this.t_TYPEBindingNavigatorSaveItem.Name = "t_TYPEBindingNavigatorSaveItem";
            this.t_TYPEBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_TYPEBindingNavigatorSaveItem.Text = "Save Data";
            this.t_TYPEBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_TYPEBindingNavigatorSaveItem_Click);
            // 
            // t_TYPEDataGridView
            // 
            this.t_TYPEDataGridView.AutoGenerateColumns = false;
            this.t_TYPEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_TYPEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.t_TYPEDataGridView.DataSource = this.t_TYPEBindingSource;
            this.t_TYPEDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_TYPEDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_TYPEDataGridView.Name = "t_TYPEDataGridView";
            this.t_TYPEDataGridView.Size = new System.Drawing.Size(641, 367);
            this.t_TYPEDataGridView.TabIndex = 1;
            this.t_TYPEDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.t_TYPEDataGridView_CellEndEdit);
            this.t_TYPEDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.t_TYPEDataGridView_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TYPE_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "TYPE_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TYPE_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Вид работы";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // TypesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 392);
            this.Controls.Add(this.t_TYPEDataGridView);
            this.Controls.Add(this.t_TYPEBindingNavigator);
            this.Name = "TypesForm";
            this.Text = "Виды работ";
            this.Load += new System.EventHandler(this.TypesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEBindingNavigator)).EndInit();
            this.t_TYPEBindingNavigator.ResumeLayout(false);
            this.t_TYPEBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_TYPEDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TasksDataSet tasksDataSet;
        private System.Windows.Forms.BindingSource t_TYPEBindingSource;
        private TasksDataSetTableAdapters.T_TYPETableAdapter t_TYPETableAdapter;
        private TasksDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_TYPEBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_TYPEBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView t_TYPEDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}
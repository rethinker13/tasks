﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks
{
    public partial class AddPerformerForm : Form
    {
        private TasksDataSetTableAdapters.T_PERFORMERTableAdapter performerAdapter;

        public AddPerformerForm ()
        {
            InitializeComponent();

            performerAdapter = new TasksDataSetTableAdapters.T_PERFORMERTableAdapter();
            performerAdapter.ClearBeforeFill = true;
        }

        private void AddPerformerForm_Load (object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'tasksDataSet.T_POSITION' table. You can move, or remove it, as needed.
                this.t_POSITIONTableAdapter.Fill(this.tasksDataSet.T_POSITION);
                this.performerAdapter.Fill(this.tasksDataSet.T_PERFORMER);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void btnCancel_Click (object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click (object sender, EventArgs e)
        {
            try
            {
                long position = (long)cmbPosition.SelectedValue;
                performerAdapter.Insert(txtName.Text, (int)position);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
                this.DialogResult = DialogResult.Cancel;
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}

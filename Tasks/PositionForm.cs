﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks
{
    public partial class PositionForm : Form
    {
        public PositionForm ()
        {
            InitializeComponent();
        }

        private void t_POSITIONBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.t_POSITIONBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.tasksDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void PositionForm_Load (object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'tasksDataSet.T_TYPE' table. You can move, or remove it, as needed.
                this.t_TYPETableAdapter.Fill(this.tasksDataSet.T_TYPE);
                // TODO: This line of code loads data into the 'tasksDataSet.T_POSITION' table. You can move, or remove it, as needed.
                this.t_POSITIONTableAdapter.Fill(this.tasksDataSet.T_POSITION);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void t_POSITIONDataGridView_DataError (object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                "Ошибка", e.Exception.Message);
            message.ShowDialog();
        }

        private void tsbPositionTypes_Click (object sender, EventArgs e)
        {
            if (t_POSITIONDataGridView.CurrentRow != null)
            {
                DataGridViewRow row = t_POSITIONDataGridView.CurrentRow;
                long positionID = (long)row.Cells[0].Value;
                string position = row.Cells[1].Value.ToString();
                if (String.IsNullOrEmpty(position))
                    return;
                PositionTypesForm positionTypesForm = new PositionTypesForm(position, positionID);
                positionTypesForm.ShowDialog();
            }
        }

        private void t_POSITIONDataGridView_CellEndEdit (object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Object obj = t_POSITIONDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                if (obj != null)
                {
                    if (String.IsNullOrEmpty(obj.ToString().Trim()))
                    {
                        MessageBox.Show("Поле не может быть пустым!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        t_POSITIONDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
                    }
                }
                else
                {
                    MessageBox.Show("Поле не может быть пустым!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }
    }
}

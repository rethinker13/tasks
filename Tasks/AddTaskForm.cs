﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks
{
    public partial class AddTaskForm : Form
    {
        private TasksDataSetTableAdapters.T_TASKTableAdapter taskAdapter;

        private int _category;
        private string _theme;
        private string _price;
        private decimal _time;
        private int _taskID;

        public int Category
        {
            get
            {
                return _category;
            }

            set
            {
                _category = value;
            }
        }

        public string Theme
        {
            get
            {
                return _theme;
            }

            set
            {
                _theme = value;
            }
        }

        public string Price
        {
            get
            {
                return _price;
            }

            set
            {
                _price = value;
            }
        }

        public decimal Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
            }
        }

        public int TaskID
        {
            get
            {
                return _taskID;
            }

            set
            {
                _taskID = value;
            }
        }

        public AddTaskForm ()
        {
            InitializeComponent();

            taskAdapter = new TasksDataSetTableAdapters.T_TASKTableAdapter();
            taskAdapter.ClearBeforeFill = true;
        }

        private void btnAddTask_Click (object sender, EventArgs e)
        {
            try
            {
                decimal price = 0;
                try
                {
                    price = decimal.Parse(txtPrice.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Введите корректную цену!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return;
                }
                if (price == 0)
                {
                    MessageBox.Show("Цена не может быть равной 0!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return;
                }
                try
                {
                    _time = decimal.Parse(txtTime.Text);                 
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Введите корректное время  работы!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return;
                }
                if (_time == 0)
                {
                    MessageBox.Show("Время работ не может быть равно 0!", "Неверный формат", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return;
                }
                string desc = txtDesc.Text;
                string theme = txtTheme.Text;
                if (String.IsNullOrEmpty(theme))
                {
                    MessageBox.Show("Тема не может быть пустой!", "Неверный формат", MessageBoxButtons.OK,
                           MessageBoxIcon.Error);
                    return;
                }

                _category = (int)(long)cmbType.SelectedValue;

                object obj = this.taskAdapter.SelectMaxID();
                if (obj != null)
                {
                    _taskID = (int)(long)obj + 1;
                }
                else
                {
                    _taskID = 1;
                }

                this.taskAdapter.Insert(price, desc, _time, (int)_category, 0, theme, _taskID);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
                //this.DialogResult = DialogResult.Cancel;
                //return;
            }


        }

        private void AddTaskForm_Load (object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'tasksDataSet.T_TYPE' table. You can move, or remove it, as needed.
                this.t_TYPETableAdapter.Fill(this.tasksDataSet.T_TYPE);

                this.taskAdapter.Fill(this.tasksDataSet.T_TASK);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void txtPrice_KeyPress (object sender, KeyPressEventArgs e)
        {
            // запрет на ввод букв
            if (!Char.IsDigit(e.KeyChar) && (e.KeyChar != '\b') && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void txtTime_KeyPress (object sender, KeyPressEventArgs e)
        {
            // запрет на ввод букв
            if (!Char.IsDigit(e.KeyChar) && (e.KeyChar != '\b') && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks
{
    public partial class PositionTypesForm : Form
    {
        private long _positionID;

        public PositionTypesForm (string position, long positionID)
        {
            InitializeComponent();

            this.Text += "[" + position + "]";
            this._positionID = positionID;
        }

        private void t_POSITION_TYPEBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.t_POSITION_TYPEBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.tasksDataSet);

                updateComboboxes();
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void PositionTypesForm_Load (object sender, EventArgs e)
        {
            try
            {
                // TODO: This line of code loads data into the 'tasksDataSet.T_TYPE' table. You can move, or remove it, as needed.
                this.t_TYPETableAdapter.Fill(this.tasksDataSet.T_TYPE);
                // TODO: This line of code loads data into the 'tasksDataSet.T_POSITION_TYPE' table. You can move, or remove it, as needed.
                this.t_POSITION_TYPETableAdapter.FillByPositionID(this.tasksDataSet.T_POSITION_TYPE, (int)_positionID);

                updateComboboxes();
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void t_POSITION_TYPEDataGridView_CellEndEdit (object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Object obj = t_POSITION_TYPEDataGridView.Rows[e.RowIndex].Cells[2].Value;
                if (obj != null)
                {
                    t_POSITION_TYPEDataGridView.Rows[e.RowIndex].Cells[1].Value = _positionID;

                }
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    "Ошибка", ex.Message);
                message.ShowDialog();
            }
        }

        private void t_POSITION_TYPEDataGridView_DataError (object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                "Ошибка", e.Exception.Message);
            message.ShowDialog();
        }

        private void updateComboboxes ()
        {
            //try
            //{
            //    foreach (DataGridViewRow row in t_POSITION_TYPEDataGridView.Rows)
            //    {
            //        if (row.Cells[1].Value != null)
            //            ((DataGridViewComboBoxCell)row.Cells["dataGridViewTextBoxColumn3"]).Value = row.Cells[1].Value;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
            //        "Ошибка", ex.Message);
            //    message.ShowDialog();
            //}
        }

        private void t_POSITION_TYPEDataGridView_CellBeginEdit (object sender, DataGridViewCellCancelEventArgs e)
        {
            t_POSITION_TYPEDataGridView.Rows[e.RowIndex].Cells[1].Value = _positionID;
        }
    }
}
